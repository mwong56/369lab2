import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by micha on 1/18/2016.
 */
public class KVTester {

  private static final KeyValueStore kvStore = new KeyValueStore();
  private static KVCollection kvCollection;

  public static void main(String[] args) {
    kvStore.addCollection("test");
    kvCollection = kvStore.getCollection("test");

    JSONObject object = new JSONObject();
    object.put("name", 1);
    object.put("grade", "A");
    kvCollection.put(0, object);

    JSONObject obj2 = new JSONObject();
    object.put("name", 1);
    object.put("grade", "B");
    kvCollection.put(5, object);

    JSONArray ar = kvCollection.find("name", 1);
    ar.length();

  }
}
