import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/*
  Created by Michael Wong and Timothy Chu and Terrence Li
 */
public class Experiment2 {
  private static File wordsList;
  private static final int wordsListLength = 6833;

  private static final KeyValueStore kvStore = new KeyValueStore();
  private static KVCollection kvCollection;
  private static int count = 1;


  public static void main(String[] args) {
    //OOM at 784944 objects
    wordsList = new File("sense.txt");
    ArrayList<String> dictionary = createDictionary();
    kvStore.addCollection("test");
    kvCollection = kvStore.getCollection("test");

    generateLogs(dictionary);
    System.out.println("Done.");
  }

  public static ArrayList<String> createDictionary() {
    ArrayList<String> toReturn = new ArrayList<>();
    Scanner scan = null;
    try {
      scan = new Scanner(wordsList);
    } catch (Exception e) {
      System.err.println("Invalid dictionary file.");
      System.exit(0);
    }

    for (int i = 0; i < wordsListLength; i++) {
      String x = scan.next();
      toReturn.add(x);
    }
    return toReturn;
  }

  private static void print(MessageLog log) {
    try {
      int status = kvCollection.put(count++, log.toJSONObject());
    } catch (OutOfMemoryError e) {
      System.out.println("OOM at " + (count) + " objects");
      System.exit(0);
    }
  }

  public static void generateLogs(ArrayList<String> dictionary) {
    try {
      while (true) {
        MessageLog log = new MessageLog(dictionary);
        print(log);
      }
    } catch (OutOfMemoryError e) {
      System.out.println("OOM at " + (count) + " objects");
      System.exit(0);
    }
  }
}
