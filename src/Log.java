import org.json.JSONObject;

/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Log object.
 */
class Log {
  private Integer user;
  private Integer gameId;
  private JSONObject action;
  private int actionCount = 0;
  private int totalPoints = 0;

  public Log(Log log, JSONObject action, int points, int actionCount) {
    this(log.getUser(), log.getGameId(), action, points, actionCount);
  }

  public Log(Integer user, Integer gameId, JSONObject action, int points, int actionCount) {
    this.user = user;
    this.gameId = gameId;
    this.action = action;
    this.totalPoints = points;
    this.actionCount = actionCount;
  }

  public Integer getUser() {
    return user;
  }

  public Integer getGameId() {
    return gameId;
  }

  public int getActionCount() {
    return actionCount;
  }

  public int getTotalPoints() {
    return totalPoints;
  }

  public JSONObject toJSONObject() {
    JSONObject toReturn = new JSONObject();
    toReturn.put("user", "u" + user);
    toReturn.put("game", gameId);
    toReturn.put("action", action);
    return toReturn;
  }
}
