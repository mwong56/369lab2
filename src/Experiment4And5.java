import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;

/*
  Created by Michael Wong and Timothy Chu and Terrence Li
 */
public class Experiment4And5 {
  private static Random rand = new Random();

  public static void main(String args[]) {

        /*
         * Experiment 4 - how fast can you retrieve a single JSON object by its
         * key from a single collection, based on the size of the collection?
         */
    System.out.println("------------------------------");
    System.out.println("TESTING KVCollection.get(index key)\n");
    KVCollection kvCollection = null;

    testSingleRetrieval(kvCollection, 100); //testing retrieval with collection of size 100
    testSingleRetrieval(kvCollection, 1000); //testing retrieval with collection of size 1000
    testSingleRetrieval(kvCollection, 100000); //testing retrieval with collection of size 100000
    //testSingleRetrieval(kvCollection, 1000000); //testing retrieval with collection of size 1000000

        /*
         * Experiment 5 - how fast can you find what JSON objects in a single collection
         * possess a certain property (based on the size of the collection? (That is, how fast in your
         * find() implementation?). Experiment with objects of both types.
         */
    System.out.println("\n------------------------------");
    System.out.println("TESTING KVCollection.find(String jsonFieldName, Object jsonFieldValue)\n");

    System.out.println("BEFUDDLED");
    System.out.println("        Finding nonexistent key-value pairs");
    //testing find(String, String) with collection of size 100 - where no match exists - Befuddled
    testFind(kvCollection, 100, true, "game", "1");
    //testing find(String, String) with collection of size 1000 - where no match exists - Befuddled
    testFind(kvCollection, 1000, true, "game", "1");
    //testing find(String, String) with collection of size 100000 - where no match exists - Befuddled
    testFind(kvCollection, 100000, true, "game", "1");

    System.out.println("\n        Finding existing key-value pairs");
    //testing find(String, Integer) with collection of size 100 - where matches exist - Befuddled
    testFind(kvCollection, 100, true, "game", 1);
    //testing find(String, Integer) with collection of size 1000 - where matches exist - Befuddled
    testFind(kvCollection, 1000, true, "game", 1);
    //testing find(String, Integer) with collection of size 100000 - where matches exist - Befuddled
    testFind(kvCollection, 100000, true, "game", 1);


    System.out.println("\nTHGHTSHRE");
    System.out.println("        Finding nonexistent key-value pairs");
    //testing find(String, String) with collection of size 100 - where no match exists - ThghtShre
    testFind(kvCollection, 100, false, "recepient", 1);
    //testing find(String, String) with collection of size 1000 - where no match exists - ThghtShre
    testFind(kvCollection, 1000, false, "recepient", 1);
    //testing find(String, String) with collection of size 100000 - where no match exists - ThghtShre
    testFind(kvCollection, 100000, false, "recepient", 1);

    System.out.println("\n        Finding existing key-value pairs");
    //testing find(String, Integer) with collection of size 100 - where matches exist - ThghtShre
    testFind(kvCollection, 100, false, "recepient", "subscribers");
    //testing find(String, Integer) with collection of size 1000 - where matches exist - ThghtShre
    testFind(kvCollection, 1000, false, "recepient", "subscribers");
    //testing find(String, Integer) with collection of size 100000 - where matches exist - ThghtShre
    testFind(kvCollection, 100000, false, "recepient", "subscribers");
  }

  public static void testSingleRetrieval(KVCollection kvCol, int size) {
    long avg = 0;

    for (int i = 0; i < 100; i++) {
      int x = rand.nextInt(size);
      kvCol = new KVCollection();
      generateKVCollectionForGet(kvCol, size);

      long timeNow = System.nanoTime();
      kvCol.get(x);
      long timeAfter = System.nanoTime();
      avg += timeAfter - timeNow;
    }
    System.out.println("Average time out of 100 runs to retrieve json object by key from collection of size " + size + ": " + avg / (100 * 1.0) + " nanoseconds");
  }

  public static void generateKVCollectionForGet(KVCollection kvCol, int size) {
    for (int i = 0; i < size; i++) {
      JSONObject tmp = new JSONObject();
      String str = Integer.toString(rand.nextInt(9999));
      try {
        tmp.put("val", str);
      } catch (JSONException e1) {
        e1.printStackTrace();
      } catch (Throwable e) {
        System.out.println("Throwable caught");
      }
      kvCol.put(i, tmp);
    }
  }

  public static void testFind(KVCollection kvCol, int size, boolean befuddled, String jsonFieldName, Object jsonFieldValue) {
    long avg = 0;

    kvCol = new KVCollection();
    generateKVCollectionForFind(kvCol, size, befuddled);

    for (int i = 0; i < 100; i++) {
      long timeNow = System.nanoTime();
      kvCol.find(jsonFieldName, jsonFieldValue);
      long timeAfter = System.nanoTime();
      avg += timeAfter - timeNow;
    }
    System.out.println("   Average time out of 100 runs to find all jsons with supplied key-value pair from collection of size " + size + ": " + avg / (100 * 1.0) + " nanoseconds");
  }

  public static void generateKVCollectionForFind(KVCollection kvCol, int size, boolean befuddled) {
    JSONTokener tokener = null;

    // initialize tokener
    try {
      if (befuddled) {
        tokener = new JSONTokener(new FileReader("befuddledGen.txt"));
      } else {
        tokener = new JSONTokener(new FileReader("thghtShreGen.txt"));
      }
    } catch (Exception e) {
      System.out.println("Could not find pre-generated test files: befuddledGen.txt or thghtShreGen.txt.");
      System.exit(-1);
    } catch (Throwable e) {
      System.out.println("Could not find pre-generated test files: befuddledGen.txt or thghtShreGen.txt.");
      System.exit(-1);
    }

    // use tokener to fill in KVCollection with objects from Befuddled or ThghtShre
    try {
      for (int i = 0; i < size; i++) {
        if (tokener.next() != ']') {
          tokener.skipTo('{');
          JSONObject log = new JSONObject(tokener);
          kvCol.put(i, log);
//                    System.out.println("Adding: " + kvCol.get(i));
        } else {
          if (befuddled) {
            tokener = new JSONTokener(new FileReader("befuddledGen.txt"));
          } else {
            tokener = new JSONTokener(new FileReader("thghtShreGen.txt"));
          }
          if (tokener.next() != ']') {
            tokener.skipTo('{');
            JSONObject log = new JSONObject(tokener);
            kvCol.put(i, log);
//                        System.out.println("Adding: " + kvCol.get(i));
          }
        }
      }
    } catch (JSONException e) {
      System.out.println("JSONException caught while generating KVCollection to test find() method.");
      System.exit(-1);
    } catch (FileNotFoundException e) {
      System.out.println("Could not find pre-generated test files: befuddledGen.txt or thghtShreGen.txt.");
      System.exit(-1);
    } catch (Exception e) {
      System.out.println("generateKVCollectionForFind error");
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
