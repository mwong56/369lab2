import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by Michael Wong and Timothy Chu and Terrence Li
 * Driver class to print JSON objects.
 */
class Experiment1 {

  private static final int numberGames = 50;
  // Set of all users currently playing a game.
  private static final HashSet<Integer> userIdSet = new HashSet<>();
  // Map game number to log
  private static final HashMap<Integer, List<Log>> map = new HashMap<>();
  //Map game number to set of special moves.
  private static final HashMap<Integer, Set<ActionSpecial.SpecialMoveName>> specialMoveMap = new HashMap<>();
  private static final Random random = new Random();
  private static int currentGameId = 1;

  private static int count = 1;

  private static final KeyValueStore kvStore = new KeyValueStore();
  private static KVCollection kvCollection;

  public static void main(String[] args) {
    //OOM at 636642 objects
    //OOM at 603149 objects
    try {
      kvStore.addCollection("test");
      kvCollection = kvStore.getCollection("test");
      generate();
      System.out.println("Done.");
    } catch (OutOfMemoryError  e) {
      System.out.println(e.toString());
      System.out.println("OOM at " + count + " objects");
      System.exit(0);
    } finally {
      System.out.println("OOM at " + count + " objects");
      System.exit(0);
    }
  }


  private static void generate() {
    try {
      while (true) {
        int currentGameNumber = random.nextInt(Experiment1.numberGames) + 1;

        List<Log> logList; //The log list for the current game.

        // If game hasn't been started.
        if (!map.containsKey(currentGameNumber)) {
          logList = new ArrayList<>();
          Integer newUser = generateUserId();
          Log log = new Log(newUser, currentGameId++, new ActionStart().toJSONObject(), 0, 1);
          logList.add(log);
          print(log);
          userIdSet.add(newUser);
          map.put(currentGameNumber, logList);
        } else {
          logList = map.get(currentGameNumber);
          Log lastLog = logList.get(logList.size() - 1);

          // Generate special move.
          if (random.nextInt(40) < 2) {
            Set<ActionSpecial.SpecialMoveName> currentGameSpecialMoveSet;
            if (specialMoveMap.containsKey(lastLog.getGameId())) {
              currentGameSpecialMoveSet = specialMoveMap.get(lastLog.getGameId());
            } else {
              currentGameSpecialMoveSet = new HashSet<>();
              specialMoveMap.put(currentGameNumber, currentGameSpecialMoveSet);
            }

            //TODO:
            if (currentGameSpecialMoveSet.size() == 4) {
              break;
            }

            ActionSpecial.SpecialMoveName special = generateSpecialMove(currentGameSpecialMoveSet);
            currentGameSpecialMoveSet.add(special);
            ActionSpecial actionSpecial = new ActionSpecial(lastLog.getActionCount() + 1, special, lastLog.getTotalPoints());
            Log log = new Log(lastLog, actionSpecial.toJSONObject(), actionSpecial.getPoints(), actionSpecial.getActionNumber());
            logList.add(log);
            print(log);
          } else if (lastLog.getActionCount() >= 9 && lastLog.getActionCount() <= 40 && random.nextInt(10) < 2) {
            endGame(currentGameNumber, lastLog, logList);
          } else if (lastLog.getActionCount() > 40 && lastLog.getActionCount() <= 50 && random.nextInt(10) > 5) {
            endGame(currentGameNumber, lastLog, logList);
          } else if (lastLog.getActionCount() > 51 && lastLog.getActionCount() <= 70 && random.nextInt(10) > 2) {
            endGame(currentGameNumber, lastLog, logList);
          } else if (lastLog.getActionCount() > 70 && lastLog.getActionCount() <= 99 && random.nextInt(10) > 1) {
            endGame(currentGameNumber, lastLog, logList);
          } else if (lastLog.getActionCount() == 100) {
            endGame(currentGameNumber, lastLog, logList);
          } else {
            ActionRegular regular = new ActionRegular(lastLog.getActionCount() + 1, lastLog.getTotalPoints());
            Log log = new Log(lastLog, regular.toJSONObject(), regular.getPoints(), regular.getActionNumber());
            logList.add(log);
            print(log);
          }
        }
      }
    } catch (Throwable e) {
      System.out.println("OOM at " + (count - 1) + " objects");
      System.exit(0);
    }
  }

  private static void endGame(int currentGameNumber, Log temp, List<Log> logList) {
    ActionEnd end = new ActionEnd(temp.getActionCount() + 1, temp.getTotalPoints());
    Log log = new Log(temp, end.toJSONObject(), end.getPoints(), end.getActionNumber());
    logList.add(log);
    print(log);

    userIdSet.remove(temp.getUser());
    map.remove(currentGameNumber);
    specialMoveMap.remove(currentGameNumber);
  }

  private static void print(Log log) {
    try {
      int status = kvCollection.put(count++, log.toJSONObject());
    } catch (OutOfMemoryError e) {
      System.out.println("OOM at " + count + " objects");
      System.exit(0);
    } catch (Throwable e) {
      System.out.println("OOM at " + count + " objects");
      System.exit(0);
    }
    //    if (status == 0) {
//      System.out.println("OOM at " + (count - 1) + " objects");
//      System.exit(0);
//    }
  }

  private static ActionSpecial.SpecialMoveName generateSpecialMove(Set<ActionSpecial.SpecialMoveName> set) {
    int chance = random.nextInt(100);

    if (chance < 33 && !set.contains(ActionSpecial.SpecialMoveName.Shuffle)) {
      return ActionSpecial.SpecialMoveName.Shuffle;
    } else if (chance >= 33 && chance < 58 && !set.contains(ActionSpecial.SpecialMoveName.Invert)) {
      return ActionSpecial.SpecialMoveName.Invert;
    } else if (chance >= 58 && chance < 81 && !set.contains(ActionSpecial.SpecialMoveName.Clear)) {
      return ActionSpecial.SpecialMoveName.Clear;
    } else if (chance >= 81 && !set.contains(ActionSpecial.SpecialMoveName.Rotate)) {
      return ActionSpecial.SpecialMoveName.Rotate;
    } else {
      return generateSpecialMove(set);
    }
  }

  private static Integer generateUserId() {
    Integer newUser = random.nextInt(10000);
    while (userIdSet.contains(newUser)) {
      newUser = random.nextInt(10000);
    }
    return newUser;
  }
}