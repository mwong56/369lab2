import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Michael on 1/14/2016.
 */
public class KVCollection {

  private Map<Integer, JSONObject> map = new HashMap<>();

  public boolean clear() {
    try {
      map.clear();
      return true;
    } catch (Throwable e) {
      return false;
    }
  }

  public boolean containsKey(int key) {
    return map.containsKey(key);
  }

  public JSONObject get(int key) {
    return map.get(key);
  }

  public int put(int key, JSONObject value) {
    try {
      if (map.containsKey(key)) {
        return -1;
      }
      map.put(key, value);
    } catch (Throwable e) {
      return 0;
    }
    return 1;
  }

  public int remove(int key) {
    try {
      if (!map.containsKey(key)) {
        return -1;
      }
      map.remove(key);
      return 1;
    } catch (Throwable e) {
      return 0;
    }
  }

  public int replace(int key, JSONObject value) {
    try {
      if (!map.containsKey(key)) {
        return -1;
      }
      map.put(key, value);
      return 1;
    } catch (Throwable e) {
      return 0;
    }
  }

  public int size() {
    return map.size();
  }

  public int upsert(int key, JSONObject value) {
    try {
      if (!map.containsKey(key)) {
        map.put(key, value);
        return 2;
      }
      map.put(key, value);
      return 1;
    } catch (Throwable e) {
      return 0;
    }
  }

  public JSONArray find(String jsonFieldName, Object jsonFieldValue) {
    if (jsonFieldValue instanceof String) {
      return findString(jsonFieldName, (String) jsonFieldValue);
    } else if (jsonFieldValue instanceof Double) {
      return findDouble(jsonFieldName, (Double) jsonFieldValue);
    } else if (jsonFieldValue instanceof Integer) {
      return findInt(jsonFieldName, (Integer) jsonFieldValue);
    } else {
      return new JSONArray();
    }
  }

  public JSONArray findString(String jsonFieldName, String jsonFieldValue) {
    final JSONArray toReturn = new JSONArray();
    final Set<Integer> keys = map.keySet();

    for (Integer key : keys) {
      JSONObject object = map.get(key);
      if (object.has(jsonFieldName) && (object.get(jsonFieldName) instanceof String) && object.getString(jsonFieldName).equals(jsonFieldValue)) {
        JSONObject toAdd = new JSONObject();
        toAdd.put("key", key);
        toAdd.put("value", object);
        toReturn.put(toAdd);
      }
    }
    return toReturn;
  }

  public JSONArray findDouble(String jsonFieldName, double jsonFieldValue) {
    final JSONArray toReturn = new JSONArray();
    final Set<Integer> keys = map.keySet();

    for (Integer key : keys) {
      JSONObject object = map.get(key);
      if (object.has(jsonFieldName) && (object.get(jsonFieldName) instanceof Double) && object.getDouble(jsonFieldName) == jsonFieldValue) {
        JSONObject toAdd = new JSONObject();
        toAdd.put("key", key);
        toAdd.put("value", object);
        toReturn.put(toAdd);
      }
    }
    return toReturn;
  }

  public JSONArray findInt(String jsonFieldName, Integer jsonFieldValue) {
    final JSONArray toReturn = new JSONArray();
    final Set<Integer> keys = map.keySet();

    for (Integer key : keys) {
      JSONObject object = map.get(key);
      if (object.has(jsonFieldName) && (object.get(jsonFieldName) instanceof Integer) && object.getInt(jsonFieldName) == jsonFieldValue) {
        JSONObject toAdd = new JSONObject();
        toAdd.put("key", key);
        toAdd.put("value", object);
        toReturn.put(toAdd);
      }
    }
    return toReturn;
  }


}
