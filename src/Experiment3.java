import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
  Created by Michael Wong and Timothy Chu and Terrence Li
 */
public class Experiment3 {
  private static File wordsList;
  private static final int wordsListLength = 6833;

  private static final KeyValueStore kvStore = new KeyValueStore();
  private static KVCollection kvCollection;
  private static int count = 1;


  public static void main(String[] args) {
    //OOM at 784944 objects
    wordsList = new File("sense.txt");
    ArrayList<String> dictionary = createDictionary();
    kvStore.addCollection("test");
    kvCollection = kvStore.getCollection("test");

    for (int n = 1000; n <= 1000000; n *= 10) {
      kvCollection.clear();
      System.out.println("Testing " + n + " items");
      List<JSONObject> jsonObjects = generateLogs(dictionary, n);
      long curr = System.nanoTime();
      for (int i = 0; i < jsonObjects.size(); i++) {
        kvCollection.put(i, jsonObjects.get(i));
      }
      System.out.println(System.nanoTime() - curr + " nano seconds");
    }
    System.out.println("Done.");
  }

  public static ArrayList<String> createDictionary() {
    ArrayList<String> toReturn = new ArrayList<>();
    Scanner scan = null;
    try {
      scan = new Scanner(wordsList);
    } catch (Exception e) {
      System.err.println("Invalid dictionary file.");
      System.exit(0);
    }

    for (int i = 0; i < wordsListLength; i++) {
      String x = scan.next();
      toReturn.add(x);
    }
    return toReturn;
  }

  public static List<JSONObject> generateLogs(ArrayList<String> dictionary, int elements) {
    List<JSONObject> toReturn = new ArrayList<>();

    for (int i = 0; i < elements; i++) {
      MessageLog log = new MessageLog(dictionary);
      toReturn.add(log.toJSONObject());
    }

    return toReturn;
  }
}
