Lab 2 
Michael Wong
mwong56@calpoly.edu		
CPE369-01

Collaborators:
Timothy Chu <tchu01>
Terrence Li (tzli)

LAB 2
    - To compile: javac -cp json-20151123.jar *.java
    - To run: java -cp .:json-20151123.jar <Experiment>

    - Implementation Overview:
    - Implementation for KVCollection included an internal HashMap that mapped integer to JSON objects. The find()
    method was implemented with the suggestion from the specs. I used three methods that handled integers, doubles, and strings.
    
    - KeyValueStore was implemented using an internal HashMap that mapped strings to KVCollections. Limit is initialized to -1 
    if key value store is constructed with no limit. 
    

    - Experiments Conducted:
        - Experiment 1:
            - Description:
                Because Experiment 1 would run out of disk space before heap space, implementation was done by generating objects and storing
                them into the KVCollection during run time. Essentially, we generated an object, stored it, and waited for an out of memory error.
                Once we hit the out of memory error, we caught it and printed the current object count.
            - Results: On unix11, we hit OOM at 636642 objects.
            - To run:  java -cp .:json-20151123.jar Experiment1
            - Analysis: Running this took a long time to hit out of memory, I believe it is because generation takes a long time.
            
        - Experiment 2:
            - Description:
                Experiment 2 was implemented the same as experiment 1 except we generated thghtShre JSON objects instead of beFuddled.
            - Results: On unix11, we hit 784944 objects.
            - To run: java -cp .:json-20151123.jar Experiment2
            - Analysis: Experiment 2 was able to hold more objects than experiment 1. This is a good observation because experiment 1
                had bigger JSON objects.
                
        - Experiment 3:
            - Description: 
                - We implemented this experiment by generating an n amount of objects, then timing the amount of time it took to put all those objects
                into the KVCollection.
                - We started at 1000 objects and incremented by a factor of 100 until 100000 which was when we ran out of heap space.
            - To run: java -cp .:json-20151123.jar Experiment2
            - Results:
                1000 ~ 6352434 nano seconds
                10000 ~ 19533867 nano seconds (207.5% increase from prev)
                100000 ~ 33432858 nano seconds (71.2% increase from prev)
            - Analysis: It's surprising to see that after 10000 elements, the increase % dropped dramatically. We believe this is because the
                HashMap has resized() itself to a size that can handle 100000. Because of this, a lot of time is cut because the hashmap does not need to resize.
                
        - Experiment 4: 
            - Description
             - Similar to experiment 3, we measured the time it took to complete get() calls and computed the avg of those times. We generated JSON objects up to a certain size
                then timed how long it took for a get operation to complete.
             - To run: java -cp .:json-20151123.jar Experiment4And5
             - Results: 
                100 items ~ Avg 1560.12 nanoseconds
                1000 items ~ avg 2980.35 nanoseconds
                100000 items ~ Avg 13091.58 nanoseconds
             - Analysis: It's interesting to see that even though we are using a hashmap which has O(1) complexity for get(), the amount of items still increases the time it takes to get an item.
             
        - Experiment 5
            - Description
                - We implemented this experiment by calling find() on different sized collections. In our test we also included searching for objects that did not exist.
                - We calculated the time it took for the find() to complete. 
             - To run: java -cp .:json-20151123.jar Experiment4And5
             - Results: 
                BeFuddled:
                    - Non existent key-value pairs:
                        - 100 items ~ Avg 364964.08 nanoseconds
                        - 1000 ~ Avg 123405.0 nano seconds
                        - 10000 ~ Avg 2.4194989E7 nanoseconds
                    - Existing Key value pairs:
                        - 100: 213709.92 nanoseconds
                        - 1000: 438264.6 nanoseconds
                        - 100000: 2.719904017E7 nanoseconds
                Thought Share:
                    - Non existent key value pairs:
                        - 100: 132426.25 nanoseconds
                        - 1000: 599014.97 nanoseconds
                        - 100000: 3.009065009E7 nanoseconds
                    - Existing key value pairs:
                        - 100: 259198.71 nanoseconds
                        - 1000: 886385.3 nanoseconds
                        - 100000: 4.169041257E7 nanoseconds
             - Analysis: It's interesting to note here that non existent key value pairs completed faster than existing key value pairs. 
                - Additionally, Thought share times were longer than beFuddled times even though thought share json objects are smaller.
            
        - Experiment 6:
        - Description: 
            We experimented using 100 messages and 10 KV collections.
            
            The first sharding algorithm we did was essentially we took the message id and placed it in a specific KV collection.
            We chose which KV collection by assigning collections to a range of numbers (i.e KVCollection #1 owns message IDS from #1-10).
            Message ID was then used as the key to the JSONObject in the KVCollection.
            
            The second algorithm was simply placing a json object round robin into all the KVCollections we had.
        - To Run: java -cp .:json-20151123.jar Experiment6
        - Results:
            Average for range keys for 100 trials: 3761.92 nanoseconds
            Standard Deviation 1285.93186973494
            Average for round robin for 100 trials: 30010.78 nanoseconds
            Standard Deviation 14262.241147575654

            Average for range keys for 1000 trials: 6305.028 nanoseconds
            Standard Deviation 97698.76833728865
            Average for round robin for 1000 trials: 22837.21 nanoseconds
            Standard Deviation 14087.045072189554

            Average for range keys for 10000 trials: 921.8461 nanoseconds
            Standard Deviation 2896.8574656021897
            Average for round robin for 10000 trials: 3118.53 nanoseconds
            Standard Deviation 1796.3208202044536

            Average for range keys for 100000 trials: 199.40569 nanoseconds
            Standard Deviation 186.55632700506786
            Average for round robin for 100000 trials: 2864.9 nanoseconds
            Standard Deviation 1946.6545019597083
        - Analysis:
            We predicted that round robin would be slower than sharding by ranging the keys before the experiment.
            We are happy to observe that our prediction is correct. By storing objects through round robin, we have no choice
            but to search through every single collection from start to end. By ranging the keys with, we can select a specific
            KV collection which can eliminate (in our case 90%) of the KV collections that we need to search.
            

   - Reflection:
      - There are a lot of different ways you can implement a key value store. Key value stores are extremely efficient in doing get() and add() operations because all operations can be 
      done in O(1) complexitiy by using a map.
      - Searching/finding in a key value store is slow because you must iterate through all objects in the collection.
      - If you are able to shard objects into different collections, this will improve the search and find speeds.
      - Java has many optimizations in the back and it is difficult to see what optimizations are improving or decreasing the times for our experiment.
