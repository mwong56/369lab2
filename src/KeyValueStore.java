import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Michael on 1/14/2016.
 */
public class KeyValueStore {

  private Map<String, KVCollection> map = new HashMap<>();
  private int limit;

  public KeyValueStore() {
    this.limit = -1;
  }

  public KeyValueStore(int limit) {
    this.limit = limit;
  }

  public KeyValueStore(Collection<String> names) {
    for (String name : names) {
      map.put(name, new KVCollection());
    }
    this.limit = 0;
  }

  public void clear() {
    map.clear();
  }

  public int addCollection(String name) {
    try {
      if (limit == 0 || (limit != -1 && map.size() + 1 >= limit)) {
        return -2;
      }

      if (map.containsKey(name)) {
        return -1;
      }

      map.put(name, new KVCollection());
      return 1;
    } catch (Throwable e) {
      return 0;
    }
  }

  public KVCollection getCollection(String name) {
    return map.get(name);
  }

  public Set<String> list() {
    return map.keySet();
  }

  public boolean isEmpty() {
    return map.size() == 0;
  }

  public int size() {
    return map.size();
  }

  public int getNumObjects() {
    Set<String> keys = map.keySet();
    int count = 0;

    for (String key : keys) {
      count += map.get(key).size();
    }

    return count;
  }

  public int getLimit() {
    return limit;
  }
}
